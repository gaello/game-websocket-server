# Example of Game WebSocket Server using Node.JS

This repository contain a simple Game WebSocket Server. You can check how it was created!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/10/21/separate-game-logic-on-the-server-in-node-js​/

Enjoy!

---

# How to use it?

Clone the repository to your machine and open Terminal.

You will have to run these commands: 'npm install' and 'node server.js'

This should start the server on your local machine. Server should be available at ws://localhost:3000/

If you want to see implementation, go straight to [server.js](https://bitbucket.org/gaello/game-websocket-server/src/master/server.js) and [game.js](https://bitbucket.org/gaello/game-websocket-server/src/master/game.js). You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
