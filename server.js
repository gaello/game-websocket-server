
// Print info about Node.JS version
console.log(new Date() + ' | Using Node ' + process.version + ' version to run server');


// Setting up server.
const http = require('http');
const server = http.createServer();
const port = 3000;

// Needed for parsing URLs.
const url = require('url');

// Setting WebSockets
const WebSocket = require('ws');
const wss = new WebSocket.Server({ noServer: true, clientTracking: true });

// Needed to generate player ids
const uuidv4 = require('uuid/v4');

// Game logic is in a separate file.
const game = require('./game');

// Websocket connection handler.
wss.on('connection', function connection(ws, request) {
  console.log(new Date() + ' | A new client is connected.');

  // Assign player Id to connected client.
  var uuidPlayer = uuidv4();

  // Let game register the player.
  game.playerConnected(ws, uuidPlayer, request);

  // Handle all messages from users.
  ws.on('message', function(msgStr) {
    console.log('Message: '+msgStr);

    // Pass message to be interpreted by the game.
    game.interpretMessage(ws, uuidPlayer, msgStr);
  });

  // What to do when client disconnect?
  ws.on('close', function(connection) {
    console.log(new Date() + ' | Closing connection for a client.');

    // Pass that that information to the game.
    game.playerDisconnected(uuidPlayer);
  });
});

// Attach broadcaster, to all clients. No broadcast by default.
wss.broadcast = function(data) {
  console.log(new Date() + ' | Broadcasting: ' + data);
  console.log(new Date() + ' | Reaching ' + Object.keys(game.connectedPlayers).length + ' clients.');

  // Sending data to all connected clients.
  for (var playerKey in game.connectedPlayers) {
    var client = game.connectedPlayers[playerKey];
    if(client.readyState === WebSocket.OPEN)
    {
      // Sending data to the client.
      client.send(data);
    }
  }
};

// Attach connection termination.
wss.terminateConnections = function () {
    console.log(new Date() + ' | Terminating connections.');

    // Send warning to the connected clients.
    wss.broadcast('Server is terminating connection.');

    // Terminating connected players.
    for (var playerKey in game.connectedPlayers) {
      var client = game.connectedPlayers[playerKey];
      if(client.readyState === WebSocket.OPEN)
      {
        // Terminating client connection.
        client.terminate();
      }
    }

    // Stop running game server.
    game.deinitGame();
}

// HTTP Server ==> WebSocket upgrade handling:
server.on('upgrade', function upgrade(request, socket, head) {

    console.log(new Date() + ' | Upgrading http connection to wss: url = '+request.url);

    // Parsing url from the request.
    var parsedUrl = url.parse(request.url, true, true);
    const pathname = parsedUrl.pathname

    console.log(new Date() + ' | Pathname = '+pathname);

    // If path is valid connect to the websocket.
    if (pathname === '/') {
      wss.handleUpgrade(request, socket, head, function done(ws) {
        wss.emit('connection', ws, request);
      });
    } else {
      socket.destroy();
    }
});

// On establishing port listener.
server.listen(port, function() {
    console.log(new Date() + ' | Server is listening on port ' + port);

    // Start running game server.
    game.initGame(wss);
});

// Detecting interrupt signal.
process.on('SIGINT', function() {
  console.log();
  console.log(new Date() + ' | Caught interrupt signal.');

  // Terminating connections.
  wss.terminateConnections();

  // Closing server.
  wss.close(function () {
    console.log(new Date() + ' | Server is closed.');

    // Exiting process.
    process.exit();
  });
});
