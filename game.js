
module.exports = Game = {
  nameMap: {}, // dict with player keys - index by uuid to access player key
  connectedPlayers: {}, // dict with connected players - use player key to access connection

  // Initialize game logic and variables.
  initGame: function (wss) {
    console.log(new Date() + ' | Game initialized');

    // Saving reference to the websocket server.
    this.wss = wss;
  },

  // Deinitialize game logic and variables.
  deinitGame: function () {

    // Clearing dicts and references.
    nameMap = {};
    connectedPlayers = {};
    delete this.wss;
  },

  // Called when new client join the server.
  playerConnected: function (connection, uuidPlayer, request) {

      // Registering player with the session.
      let sessionMsg = {};
      sessionMsg.type = "register";
      sessionMsg.method = "register";

      // Gathering player connection key.
      let playerKey = request.headers['sec-websocket-key'];

      sessionMsg.sessionId = playerKey;
      sessionMsg.uuidPlayer = uuidPlayer;

      // Sending confirm message to the connected client.
      connection.send(JSON.stringify(sessionMsg));

      // Saving player variables in dicts.
      this.nameMap[uuidPlayer] = playerKey;
      this.connectedPlayers[playerKey] = connection;
  },

  // Called when client disconnects from the server.
  playerDisconnected: function (uuidPlayer) {

    // Removing player connection info.
    let playerKey = this.nameMap[uuidPlayer];
    delete this.connectedPlayers[playerKey];
    delete this.nameMap[uuidPlayer];
  },

  // Called when the client sends a message to the server.
  interpretMessage: function (connection, uuidPlayer, message) {

    // Trying to parse the message.
    try {
      let clientMessage = JSON.parse(message);

      switch (clientMessage.method) {
        case 'echo':
          console.log(new Date() + ' | Server echo message: ' + clientMessage.data);

          // Sends back the same message.
          connection.send(message);
          break;
        default:
          console.log(new Date() + ' | Server can\'t recognize message method: ' + clientMessage.method);

          // Unknown type of message.
      }

    }
    // Catching invalid messages.
    catch (e) {
        console.log(new Date() + ' | Server received invalid message: ' + message + '\nThrowing error: ' + e);
    }

  }
}
